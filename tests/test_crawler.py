from unittest import TestCase, main

from bs4 import BeautifulSoup

from src.crawler import Crawler, NoContentExtractedException


class TestCrawler(TestCase):

    def setUp(self):
        url = "https://blog.trello.com/team-goal-setting-tips"
        page = BeautifulSoup(self.read_sample_html(), 'html.parser')
        self.crawler = Crawler(url, page)

    def test_extract_title(self):
        title = self.crawler._extract_title()
        self.assertEqual(title, "3 Ways To Approach Goal Setting As A Team (And Actually Enjoy The Process)")

    def test_remove_inline_scripts(self):
        removed = self.crawler._remove_inline_styles()
        self.assertEqual(removed, 84)

    def test_extract_content(self):
        content = self.crawler._extract_content()
        self.assertIsNotNone(content)

    def test_no_content_raise_exception(self):
        self.crawler.page = BeautifulSoup(self.read_sample_html(html="tests/invalid.html"), "html.parser")

        with self.assertRaises(NoContentExtractedException) as context:
            self.crawler._extract_content()
        error_message = "No content crawled, checkout link https://blog.trello.com/team-goal-setting-tips"
        self.assertTrue(error_message in str(context.exception))

    @staticmethod
    def read_sample_html(html="tests/sample.html"):
        with open(html, 'r') as f:
            data = f.read()
        return data


if __name__ == '__main__':
    main()
