import json
from unittest import TestCase, main
from src.handler import get_link_from_event


class TestHandler(TestCase):

    def test_get_link_from_event(self):
        with open("tests/event.json", "r") as read_file:
            mock_event = json.load(read_file)
        link = get_link_from_event(mock_event)
        self.assertEqual(link, "https://www.google.com.br")

    def test_get(self):
        link = get_link_from_event(None)
        self.assertIsNone(link)


if __name__ == '__main__':
    main()
