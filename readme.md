**Create virtualenv:**

```virtualenv venv --python=python3```

**Start virtual env:**

```source venv/bin/activate```

**Stop virtual env:**

```deactivate```

**Run tests**

```python -m unittest -v```

**Deploy**
```serverless deploy --r sa-east-1 --s prod```
