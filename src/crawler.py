import json
import os

import requests

import boto3
sqs = boto3.client('sqs', region_name='sa-east-1')

from bs4 import BeautifulSoup


class NoContentExtractedException(Exception):
    def __init__(self, message):
        super(Exception, self).__init__(message)


class Crawler:
    def __init__(self, url, page=None):
        self.url = url
        self.page = page

    def start(self):
        self.__request()

        if self.page is None:
            return

        title = self._extract_title()
        content = self._extract_content()
        message = {"title": title, "content": content}

        self._send_content_to_sqs(json.dumps(message))

    def __request(self):
        try:
            response = requests.get(self.url, timeout=10)
        except:
            print("Connection error connecting to link {}".format(self.url))
            return
        if response.status_code is not 200:
            print("Error status {} crawling link {}".format(response.status_code, self.url))
            return None

        self.page = BeautifulSoup(response.content, 'html.parser')

    def _extract_title(self):
        return self.page.title.string

    def _remove_inline_styles(self):
        removed_scripts = 0
        for script in self.page(["style", "script"]):
            script.decompose()
            removed_scripts += 1

        return removed_scripts

    def _extract_content(self):
        self._remove_inline_styles()
        content = self.page.find("main")
        if content is None:
            content = self.page.find("body")
            if content is None:
                raise NoContentExtractedException("No content crawled, checkout link {}".format(self.url))

        return self._text_adjustments(content.get_text())

    def _text_adjustments(self, content):
        lines = (line.strip() for line in content.splitlines())
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        text = '\n'.join(chunk for chunk in chunks if chunk)
        return text

    def _send_content_to_sqs(self, message_body):
        sqs.send_message(QueueUrl=os.environ['QUEUE_URL'], MessageBody=message_body)


if __name__ == '__main__':
    crawler = Crawler("https://blog.trello.com/team-goal-setting-tips")
    crawler.start()
