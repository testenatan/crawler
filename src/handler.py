# -*- coding: utf-8 -*-

# Start sqlite3 fix
import imp
import sys

from src import Crawler

sys.modules["sqlite"] = imp.new_module("sqlite")
sys.modules["sqlite3.dbapi2"] = imp.new_module("sqlite.dbapi2")
# End sqlite3 fix


def handle(event, context):

    link = get_link_from_event(event)

    if link is None:
        print("Ignoring event without link")
        return

    print("Crawling link {}".format(link))

    Crawler(link).start()

    print("Link {} crawled".format(link))


def get_link_from_event(event):
    if event and event["Records"] and event["Records"][0] and event["Records"][0]["body"]:
        return event["Records"][0]["body"]
    else:
        return None


if __name__ == '__main__':
    handle('', '')
